﻿namespace Geometrijski_oblici {
    partial class form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            lbKvadrat = new Label();
            lbKruznica = new Label();
            lbPravokutnik = new Label();
            lbStranica = new Label();
            tbStranica = new TextBox();
            lbRadijus = new Label();
            tbRadijus = new TextBox();
            lbPravokutnikStranica1 = new Label();
            tbPravokutnikStranica1 = new TextBox();
            lbPravokutnikStranica2 = new Label();
            tbPravokutnikStranica2 = new TextBox();
            btnKvadrat = new Button();
            btnKruznica = new Button();
            btnPravokutnik = new Button();
            listboxPrikaz = new ListBox();
            SuspendLayout();
            // 
            // lbKvadrat
            // 
            lbKvadrat.AutoSize = true;
            lbKvadrat.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Point);
            lbKvadrat.Location = new Point(69, 52);
            lbKvadrat.Name = "lbKvadrat";
            lbKvadrat.Size = new Size(71, 25);
            lbKvadrat.TabIndex = 0;
            lbKvadrat.Text = "Kvadrat";
            // 
            // lbKruznica
            // 
            lbKruznica.AutoSize = true;
            lbKruznica.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Point);
            lbKruznica.Location = new Point(69, 285);
            lbKruznica.Name = "lbKruznica";
            lbKruznica.Size = new Size(77, 25);
            lbKruznica.TabIndex = 1;
            lbKruznica.Text = "Kružnica";
            // 
            // lbPravokutnik
            // 
            lbPravokutnik.AutoSize = true;
            lbPravokutnik.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Point);
            lbPravokutnik.Location = new Point(69, 499);
            lbPravokutnik.Name = "lbPravokutnik";
            lbPravokutnik.Size = new Size(105, 25);
            lbPravokutnik.TabIndex = 2;
            lbPravokutnik.Text = "Pravokutnik";
            // 
            // lbStranica
            // 
            lbStranica.AutoSize = true;
            lbStranica.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lbStranica.Location = new Point(69, 96);
            lbStranica.Name = "lbStranica";
            lbStranica.Size = new Size(68, 21);
            lbStranica.TabIndex = 3;
            lbStranica.Text = "Stranica:";
            // 
            // tbStranica
            // 
            tbStranica.BorderStyle = BorderStyle.FixedSingle;
            tbStranica.Location = new Point(69, 120);
            tbStranica.Name = "tbStranica";
            tbStranica.Size = new Size(204, 23);
            tbStranica.TabIndex = 4;
            // 
            // lbRadijus
            // 
            lbRadijus.AutoSize = true;
            lbRadijus.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lbRadijus.Location = new Point(72, 327);
            lbRadijus.Name = "lbRadijus";
            lbRadijus.Size = new Size(64, 21);
            lbRadijus.TabIndex = 5;
            lbRadijus.Text = "Radijus:";
            // 
            // tbRadijus
            // 
            tbRadijus.BorderStyle = BorderStyle.FixedSingle;
            tbRadijus.Location = new Point(72, 351);
            tbRadijus.Name = "tbRadijus";
            tbRadijus.Size = new Size(204, 23);
            tbRadijus.TabIndex = 6;
            // 
            // lbPravokutnikStranica1
            // 
            lbPravokutnikStranica1.AutoSize = true;
            lbPravokutnikStranica1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lbPravokutnikStranica1.Location = new Point(72, 544);
            lbPravokutnikStranica1.Name = "lbPravokutnikStranica1";
            lbPravokutnikStranica1.Size = new Size(81, 21);
            lbPravokutnikStranica1.TabIndex = 7;
            lbPravokutnikStranica1.Text = "Stranica 1:";
            // 
            // tbPravokutnikStranica1
            // 
            tbPravokutnikStranica1.BorderStyle = BorderStyle.FixedSingle;
            tbPravokutnikStranica1.Location = new Point(72, 568);
            tbPravokutnikStranica1.Name = "tbPravokutnikStranica1";
            tbPravokutnikStranica1.Size = new Size(204, 23);
            tbPravokutnikStranica1.TabIndex = 8;
            // 
            // lbPravokutnikStranica2
            // 
            lbPravokutnikStranica2.AutoSize = true;
            lbPravokutnikStranica2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lbPravokutnikStranica2.Location = new Point(72, 617);
            lbPravokutnikStranica2.Name = "lbPravokutnikStranica2";
            lbPravokutnikStranica2.Size = new Size(81, 21);
            lbPravokutnikStranica2.TabIndex = 9;
            lbPravokutnikStranica2.Text = "Stranica 2:";
            // 
            // tbPravokutnikStranica2
            // 
            tbPravokutnikStranica2.BorderStyle = BorderStyle.FixedSingle;
            tbPravokutnikStranica2.Location = new Point(72, 641);
            tbPravokutnikStranica2.Name = "tbPravokutnikStranica2";
            tbPravokutnikStranica2.Size = new Size(204, 23);
            tbPravokutnikStranica2.TabIndex = 10;
            // 
            // btnKvadrat
            // 
            btnKvadrat.Location = new Point(69, 178);
            btnKvadrat.Name = "btnKvadrat";
            btnKvadrat.Size = new Size(204, 46);
            btnKvadrat.TabIndex = 11;
            btnKvadrat.Text = "Dodaj kvadrat";
            btnKvadrat.UseVisualStyleBackColor = true;
            btnKvadrat.Click += btnKvadrat_Click;
            //btnKvadrat.Click += new EventHandler(Button_click);
            // 
            // btnKruznica
            // 
            btnKruznica.Location = new Point(69, 396);
            btnKruznica.Name = "btnKruznica";
            btnKruznica.Size = new Size(204, 46);
            btnKruznica.TabIndex = 12;
            btnKruznica.Text = "Dodaj kružnica";
            btnKruznica.UseVisualStyleBackColor = true;
            btnKruznica.Click += btnKruznica_Click;
            //btnKruznica.Click += new EventHandler(Button_click);
            // 
            // btnPravokutnik
            // 
            btnPravokutnik.Location = new Point(72, 693);
            btnPravokutnik.Name = "btnPravokutnik";
            btnPravokutnik.Size = new Size(204, 46);
            btnPravokutnik.TabIndex = 13;
            btnPravokutnik.Text = "Dodaj pravokutnik";
            btnPravokutnik.UseVisualStyleBackColor = true;
            btnPravokutnik.Click += btnPravokutnik_Click;
            //btnPravokutnik.Click += new EventHandler(Button_click);
            // 
            // listboxPrikaz
            // 
            listboxPrikaz.BorderStyle = BorderStyle.FixedSingle;
            listboxPrikaz.Font = new Font("Segoe UI", 13F, FontStyle.Regular, GraphicsUnit.Point);
            listboxPrikaz.FormattingEnabled = true;
            listboxPrikaz.ItemHeight = 23;
            listboxPrikaz.Location = new Point(397, 52);
            listboxPrikaz.Name = "listboxPrikaz";
            listboxPrikaz.Size = new Size(473, 692);
            listboxPrikaz.TabIndex = 14;
            // 
            // form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(911, 781);
            Controls.Add(listboxPrikaz);
            Controls.Add(btnPravokutnik);
            Controls.Add(btnKruznica);
            Controls.Add(btnKvadrat);
            Controls.Add(tbPravokutnikStranica2);
            Controls.Add(lbPravokutnikStranica2);
            Controls.Add(tbPravokutnikStranica1);
            Controls.Add(lbPravokutnikStranica1);
            Controls.Add(tbRadijus);
            Controls.Add(lbRadijus);
            Controls.Add(tbStranica);
            Controls.Add(lbStranica);
            Controls.Add(lbPravokutnik);
            Controls.Add(lbKruznica);
            Controls.Add(lbKvadrat);
            Name = "form1";
            Text = "Geometrijski oblik";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lbKvadrat;
        private Label lbKruznica;
        private Label lbPravokutnik;
        private Label lbStranica;
        private TextBox tbStranica;
        private Label lbRadijus;
        private TextBox tbRadijus;
        private Label lbPravokutnikStranica1;
        private TextBox tbPravokutnikStranica1;
        private Label lbPravokutnikStranica2;
        private TextBox tbPravokutnikStranica2;
        private Button btnKvadrat;
        private Button btnKruznica;
        private Button btnPravokutnik;
        private ListBox listboxPrikaz;
    }
}