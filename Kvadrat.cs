﻿
namespace Geometrijski_oblici; 
internal class Kvadrat : GeometrijskiOblik{
    public Kvadrat(float size) : base(size) { }

    public override float calculation() {
        return (float)Math.Pow(Size, 2);
    }
    public override string ToString() {
        return string.Format("Kvadrat({0}): Povrsina {1}", Size, calculation()); ;
    }
}
