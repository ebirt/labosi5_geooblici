﻿

namespace Geometrijski_oblici;
internal abstract class GeometrijskiOblik {
    public GeometrijskiOblik(float size) {
        
        Size = size; //size nije dobro ime, bolje da je povrsina ili tome slicno
    }

    public abstract float calculation();
    public float Size { get; set; }
}

