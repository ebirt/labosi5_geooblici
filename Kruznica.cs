﻿namespace Geometrijski_oblici; 
internal class Kruznica : GeometrijskiOblik {
    public Kruznica(float size) : base(size) { }

    public override float calculation() {
        return float.Pi * (float)Math.Pow(Size, 2);
    }
    public override string ToString() {
        return string.Format("Kruznica({0}): Povrsina {1}", Size, calculation());
    }
}
