namespace Geometrijski_oblici {
    public partial class form1 : Form {
        Oblici oblici;

        public form1() {
            InitializeComponent();
            oblici = new Oblici();
        }
        /*
        private void Button_click(object sender, EventArgs e) {
            Button clickedButton = (Button)sender;

            switch (clickedButton.Name) {
                case "btnKvadrat":
                    oblici.DodajOblik(new Kvadrat(float.Parse(tbStranica.Text)));
                    RefreshList();
                    break;

                case "btnKruznica":
                    oblici.DodajOblik(new Kruznica(float.Parse(tbRadijus.Text)));
                    RefreshList();
                    break;

                case "btnPravokutnik":
                    oblici.DodajOblik(new Pravokutnik(float.Parse(tbPravokutnikStranica1.Text),
                       float.Parse(tbPravokutnikStranica2.Text)));
                    RefreshList();
                    break;
            }
        }
        */
        
        private void btnKvadrat_Click(object sender, EventArgs e) {
            oblici.DodajOblik(new Kvadrat(float.Parse(tbStranica.Text)));
            RefreshList();
        }

        private void btnKruznica_Click(object sender, EventArgs e) {
            oblici.DodajOblik(new Kruznica(float.Parse(tbRadijus.Text)));
            RefreshList();
        }

        private void btnPravokutnik_Click(object sender, EventArgs e) {
            oblici.DodajOblik(new Pravokutnik(float.Parse(tbPravokutnikStranica1.Text),
                float.Parse(tbPravokutnikStranica2.Text)));
            RefreshList();
        }

        private void RefreshList() { //List box doesnt support multiline
            listboxPrikaz.Items.Clear();
            listboxPrikaz.Items.AddRange(oblici.VratiOblike().ToArray());
           
        }

    }
}