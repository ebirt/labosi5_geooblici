﻿
namespace Geometrijski_oblici; 
internal class Pravokutnik : GeometrijskiOblik {
    public Pravokutnik(float size, float size2) : base(size) { 
        Size2 = size2;
    }

    public override float calculation() {
        return Size * Size2;
    }
    public override string ToString() {
        return string.Format("Pravokutnik({0}): Povrsina {1}", Size, calculation());
    }

    public float Size2 { get; set; }
}
