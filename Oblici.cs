﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geometrijski_oblici; 
internal class Oblici {
    public Oblici() {
        sviOblici = new List<GeometrijskiOblik> { };
    }

    public void DodajOblik(GeometrijskiOblik oblik) {
        sviOblici.Add(oblik);
    }

    public List<GeometrijskiOblik> VratiOblike() {
        var allOglasi = new List<GeometrijskiOblik>();
        foreach (var item in sviOblici) {
            allOglasi.Add(item);
        }
        return allOglasi;
    }

    //public List<IGeometrijskiOblik> SviOblici { get; set; }
    private List<GeometrijskiOblik> sviOblici;
}
